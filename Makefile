swag:
	swag init -g ./api/router.go -o api/docs
run:
	go run cmd/main.go	
migrateup:
	migrate -source file://migrations -database postgres://postgres:sdy12197@localhost:5432/blog_app_db?sslmode=disable up	
migratedown:
	migrate -source file://migrations -database postgres://postgres:sdy12197@localhost:5432/blog_app_db?sslmode=disable down	