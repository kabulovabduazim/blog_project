package postgres

import (
	"fmt"

	//"github.com/go-playground/locales/ur"
	"github.com/jmoiron/sqlx"
	"gitlab.com/blog_project/storage/repo"
)

type userRepo struct {
	db *sqlx.DB
}

func NewUser(db *sqlx.DB) repo.UserStorageI {
	return &userRepo{
		db: db,
	}
}

func (r *userRepo) CheckField(req *repo.CheckFieldReq) (*repo.CheckFieldRes, error) {
	fmt.Println(req)
	query := fmt.Sprintf("SELECT 1 FROM users WHERE %s=$1", req.Field)
	res := &repo.CheckFieldRes{}
	temp := -1
	fmt.Println(query, req.Value)
	err := r.db.QueryRow(query, req.Value).Scan(&temp)
	fmt.Println(err)	
	if err != nil {
		fmt.Println("\n Error>>> ", err, "\n")
		fmt.Println(temp)
		res.Exist = false
		fmt.Println(res)
		return res, nil
	}
	if temp == 0 {
		fmt.Println(temp, res, err)
		res.Exist = true
	} else {
		res.Exist = false
	}
	fmt.Println(res)
	return res, nil
}

func(u *userRepo) Create(user *repo.User) (*repo.User, error){
	fmt.Println(user)
	query := `INSERT INTO users(
		first_name,
		last_name,
		phone_number,
		email,
		gender,
		password,
		refresh_token,
		username,
		profile_image_url,
		type
	) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) RETURNING id, created_at
	`

	row:= u.db.QueryRow(query, 
		user.FirstName, 
		user.LastName, 
		user.PhoneNumber, 
		user.Email, 
		user.Gender, 
		user.Password, 
		user.AccessToken,
		user.Username, 
		user.ProfileImageUrl, 
		user.Type)
	fmt.Println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
	err:=row.Scan(&user.ID, &user.CreatedAt)
	fmt.Println(err)
	if err != nil {
		return &repo.User{}, err
	}

	fmt.Println("created",user)
	return user, nil
}

func(u *userRepo) Get(id int64) (*repo.User, error){
	var user repo.User
	query:= `
	SELECT 
		id,
		first_name,
		last_name,
		phone_number,
		email,
		gender,
		password,
		username,
		profile_image_url,
		type,
		created_at
		FROM users
		WHERE id=$1
		`

	row := u.db.QueryRow(query, id)
	err := row.Scan(&user.ID, 
		&user.FirstName, 
		&user.LastName, 
		&user.PhoneNumber, 
		&user.Email, 
		&user.Gender, 
		&user.Password, 
		&user.Username, 
		&user.ProfileImageUrl,
	    &user.Type)
	if err != nil {
		return &repo.User{}, nil 
	}	

	return &user, nil

}

func(u *userRepo) GetByEmail(email string) (*repo.User, error){
	result:=repo.User{}

	query:=`SELECT 
	id,
	first_name,
	last_name,
	phone_number,
	email,
	gender,
	password,
	username,
	profile_image_url,
	type,
	created_at
	FROM users
	WHERE email=$1`

	row:=u.db.QueryRow(query, email)
	err:=row.Scan(&result.ID, 
		&result.FirstName, 
		&result.LastName, 
		&result.PhoneNumber, 
		&result.Email, 
		&result.Gender, 
		&result.Password, 
		&result.Username, 
		&result.ProfileImageUrl,
		&result.Type)
	if err != nil{
		return &repo.User{}, err
	}	
	
	return &result, nil
} 


func (u *userRepo) GetAll(params *repo.GetAllUsersParams) (*repo.GetAllUsersResult, error) {
	result := repo.GetAllUsersResult{
		Users: make([]*repo.User, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d ", params.Limit, offset)

	filter := ""
	if params.Search != "" {
		str := "%" + params.Search + "%"
		filter += fmt.Sprintf(`
			WHERE first_name ILIKE '%s' OR last_name ILIKE '%s' OR email ILIKE '%s' 
				OR username ILIKE '%s' OR phone_number ILIKE '%s'`,
			str, str, str, str, str,
		)
	}

	query := `
		SELECT
			id,
			first_name,
			last_name,
			phone_number,
			email,
			gender,
			password,
			username,
			profile_image_url,
			type,
			created_at
		FROM users
		` + filter + `
		ORDER BY created_at desc
		` + limit

	rows, err := u.db.Query(query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var u repo.User

		err := rows.Scan(
			&u.ID,
			&u.FirstName,
			&u.LastName,
			&u.PhoneNumber,
			&u.Email,
			&u.Gender,
			&u.Password,
			&u.Username,
			&u.ProfileImageUrl,
			&u.Type,
			&u.CreatedAt,
		)
		if err != nil {
			return nil, err
		}

		result.Users = append(result.Users, &u)
	}

	queryCount := `SELECT count(1) FROM users ` + filter
	err = u.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return nil, err
	}

	return &result, nil
}


func(u *userRepo) UpdatePassword(req *repo.UpdatePassword) (error){
	query:=`UPDATED users SET password=$1 WHERE id=$2`
	_, err:=u.db.Exec(query, req.Password, req.UserID)
	if err != nil{
		return err
	} 
	return nil
}