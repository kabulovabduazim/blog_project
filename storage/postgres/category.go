package postgres

import (
	"fmt"
	"database/sql"
	"github.com/jmoiron/sqlx"
	"gitlab.com/blog_project/storage/repo"
)

type categoryRepo struct {
	db *sqlx.DB
}

func NewCategory(db *sqlx.DB) repo.CategoryStorageI {
	return &categoryRepo{
		db: db,
	}
}

func(c *categoryRepo) Create(ct *repo.Category) (*repo.Category, error){
	query:=`INSERT INTO categories(title) VALUES ($1) RETURNING id, title, created_at`
	rows:=c.db.QueryRow(query, ct.Title)
	err:=rows.Scan(&ct.ID, &ct.Title, &ct.CreatedAt)
	if err != nil {
		return &repo.Category{}, err
	}
	return ct, nil
}

func(c *categoryRepo) Get(id int64) (*repo.Category, error){
	result:=repo.Category{}

	query:=`SELECT 
		id, 
		title, 
		created_at 
		FROM categories WHERE id=$1`
	rows:=c.db.QueryRow(query, id)
	err:=rows.Scan(&result.ID, &result.Title, &result.CreatedAt)
	if err != nil {
		return &repo.Category{}, nil
	}	
	return &result, nil
}

func(c *categoryRepo) Update(ct *repo.Category) (*repo.Category, error){
	query:=`UPDATE categories SET title=$1 WHERE id=$2 RETURNING created_at`
	rows:=c.db.QueryRow(query, ct.Title, ct.ID)
	err:=rows.Scan(ct.CreatedAt)
	if err != nil {
		return &repo.Category{}, err
	}
	return ct, nil
}

func(c *categoryRepo) Delete(id int64) (error){
	query:=`DELETE FROM categories WHERE id=$1`
	
	result, err := c.db.Exec(query, id)
	if err != nil {
		return err
	}

	rowsEffected, err := result.RowsAffected()
	if err != nil {
		return err
	}

	if rowsEffected == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (c *categoryRepo) GetAll(params *repo.GetAllCategoriesParams) (*repo.GetAllCategoriesResult, error) {
	result := repo.GetAllCategoriesResult{
		Categories: make([]*repo.Category, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d ", params.Limit, offset)

	filter := ""
	if params.Search != "" {
		filter += " WHERE title ilike '%" + params.Search + "%' "
	}

	query := `
		SELECT
			id, 
			title, 
			created_at
		FROM categories
		` + filter + `
		ORDER BY created_at desc
		` + limit

	rows, err := c.db.Query(query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var c repo.Category

		err := rows.Scan(
			&c.ID,
			&c.Title,
			&c.CreatedAt,
		)
		if err != nil {
			return nil, err
		}

		result.Categories = append(result.Categories, &c)
	}

	queryCount := `SELECT count(1) FROM categories ` + filter
	err = c.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return nil, err
	}

	return &result, nil
}