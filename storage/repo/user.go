package repo

import "time"

const (
	UserTypeSuperadmin = "superadmin"
	UserTypeUser       = "user"
)

type User struct {
	ID              int64
	FirstName       string
	LastName        string
	PhoneNumber     *string
	Email           string
	Gender          *string
	Password        string
	AccessToken     string
	Username        *string
	ProfileImageUrl *string
	Type            string
	CreatedAt       time.Time
}

type GetAllUsersParams struct {
	Limit  int32
	Page   int32
	Search string
}

type GetAllUsersResult struct {
	Users []*User
	Count int32
}

type UpdatePassword struct {
	UserID   int64
	Password string
}

type CheckFieldReq struct {
	Field string
	Value string
}

type CheckFieldRes struct {
	Exist bool
}
type UserStorageI interface {
	Create(*User) (*User, error)
	Get(id int64) (*User, error)
	GetByEmail(email string) (*User, error)
	GetAll(*GetAllUsersParams) (*GetAllUsersResult, error)
	UpdatePassword(*UpdatePassword) error
	CheckField(*CheckFieldReq) (*CheckFieldRes, error)
}
