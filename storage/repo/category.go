package repo

import "time"

type Category struct {
	ID        string
	Title     string
	CreatedAt time.Time
}

type GetAllCategoriesParams struct {
	Limit  int32
	Page   int32
	Search string
}

type GetAllCategoriesResult struct {
	Categories []*Category
	Count      int32
}

type CategoryStorageI interface {
	Create(ct *Category) (*Category, error)
	Get(id int64) (*Category, error)
	GetAll(params *GetAllCategoriesParams) (*GetAllCategoriesResult, error)
	Update(ct *Category) (*Category, error)
	Delete(id int64) error
}
