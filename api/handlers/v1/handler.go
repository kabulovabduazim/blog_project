package v1

import (
	"fmt"
	"strconv"
	"gitlab.com/blog_project/api/handlers/models"
	jwthandler "gitlab.com/blog_project/api/tokens"
	t "gitlab.com/blog_project/api/tokens"
	"gitlab.com/blog_project/config"
	"gitlab.com/blog_project/pkg/logger"
	"gitlab.com/blog_project/storage/repo"

	//"gitlab.com/blog_project/services"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gitlab.com/blog_project/storage"
)

type handlerV1 struct {
	log        logger.Logger
	cfg        config.Config
	storage    storage.StorageI
	redis      repo.InMemoryStorageI
	jwthandler t.JWTHandler
}

// HandlerV1Config ...
type HandlerV1Config struct {
	Logger     logger.Logger
	Cfg        config.Config
	Storage    storage.StorageI
	Redis      repo.InMemoryStorageI
	JWTHandler t.JWTHandler
}

// New ...
func New(c *HandlerV1Config) *handlerV1 {
	return &handlerV1{
		log:        c.Logger,
		cfg:        c.Cfg,
		storage:    c.Storage,
		redis:      c.Redis,
		jwthandler: c.JWTHandler,
	}
}
func GetClaims(h handlerV1, c *gin.Context) (*jwthandler.CustomClaims, error) {

	var (
		claims = jwthandler.CustomClaims{}
	)

	strToken := c.GetHeader("Authorization")
	fmt.Println(h.cfg.SignKey)

	token, err := jwt.Parse(strToken, func(t *jwt.Token) (interface{}, error) { return []byte(h.cfg.SignKey), nil })

	if err != nil {
		fmt.Println(err)
		h.log.Error("invalid access token")
		return nil, err
	}
	// rawClaims := token.Claims.(jwt.MapClaims)

	// claims.Sub = rawClaims["sub"].(string)
	// claims.Exp = rawClaims["exp"].(float64)
	// fmt.Printf("%T type of value in map %v",rawClaims["exp"],rawClaims["exp"])
	// fmt.Printf("%T type of value in map %v",rawClaims["iat"],rawClaims["iat"])

	claims.Token = token
	return &claims, nil

}

func validateGetAllParams(c *gin.Context) (*models.GetAllParams, error) {
	var (
		limit int = 10
		page  int = 1
		err   error
	)

	if c.Query("limit") != "" {
		limit, err = strconv.Atoi(c.Query("limit"))
		if err != nil {
			return nil, err
		}
	}

	if c.Query("page") != "" {
		page, err = strconv.Atoi(c.Query("page"))
		if err != nil {
			return nil, err
		}
	}

	return &models.GetAllParams{
		Limit:  int32(limit),
		Page:   int32(page),
		Search: c.Query("search"),
	}, nil
}
