package v1

import (
	"context"
	//"gitlab.com/blog_project/api/handlers/models"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	email "gitlab.com/blog_project/email"
	"gitlab.com/blog_project/storage/repo"

	"strconv"
	"time"

	"github.com/spf13/cast"

	"github.com/gin-gonic/gin"
	etc "gitlab.com/blog_project/pkg/etc"

	us "gitlab.com/blog_project/api/handlers/models"
	l "gitlab.com/blog_project/pkg/logger"
	//"google.golang.org/protobuf/encoding/protojson"
)

// Register Custumer
// @Summary      Register User
// @Description  Registers User
// @Tags         Register
// @Security BearerAuth
// @Accept       json
// @Produce      json
// @Param        user   body   models.CreateUserRequest   true  "Custumer"
// @Success      200  {object}  models.User
// @Router       /v1/register [post]
func (h *handlerV1) RegisterUser(c *gin.Context) {
	var body us.CreateUserRequest
	err := c.ShouldBindJSON(&body)
	fmt.Println(body, err)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"Error": err.Error(),
			"Hint":  "Check your data",
		})
		h.log.Error("Error while binding json", l.Any("json", err))
		return
	}

	fmt.Println(body.FirstName)
	body.Email = strings.TrimSpace(body.Email)
	body.FirstName = strings.TrimSpace(body.FirstName)
	body.Email = strings.ToLower(body.Email)
	body.Password, err = etc.HashPassword(body.Password)
	fmt.Println(body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, "Something went wrong")
		h.log.Error("couldn't hash the password")
		return
	}

	_, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	fmt.Println(body.Email)
	//emailExists, err := h.storage.User().CheckField(&repo.CheckFieldReq{
	//	Field: "email",
	//	Value: body.Email,
	//})
	//fmt.Println(emailExists, err)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"Error": err.Error(),
		})
		h.log.Error("Error while cheking email uniqeness", l.Any("check", err))
		return
	}

	//if emailExists.Exist {
	//	c.JSON(http.StatusConflict, gin.H{
	//		"info": "Email is already used",
	//	})
	//	return
	//}
	//usernameExists, err := h.storage.User().CheckField(&repo.CheckFieldReq{
	//	Field: "username",
	//	Value: body.FirstName,
	//})
	//if err != nil {
	//	c.JSON(http.StatusInternalServerError, gin.H{
	//		"Error": err.Error(),
	//	})
	//	h.log.Error("Error while cheking username uniqeness", l.Any("check", err))
	//	return
	//}
	//if usernameExists.Exist {
	//	c.JSON(http.StatusConflict, gin.H{
	//		"info": "Username is already used",
	//	})
	//	return
	//}
	body.Code = etc.GenerateCode(6)
	bodyByte, err := json.Marshal(body)
	if err != nil {
		h.log.Error("Error while marshaling to json", l.Any("json", err))
		return
	}
	msg := "Subject: Customer email verification\n Your verification code: " + body.Code
	err = email.SendEmail([]string{body.Email}, []byte(msg))

	c.JSON(http.StatusAccepted, gin.H{
		"info": "Your request is accepted we have sent you an email message, please check and verify",
	})
	fmt.Println(body.Email)
	fmt.Println(string(bodyByte))
	fmt.Println(body.Email)
	fmt.Println(string(bodyByte))
	err = h.redis.SetWithTTL(body.Email, string(bodyByte), 300)
	fmt.Println(body.Email)
	if err != nil {
		h.log.Error("Error while marshaling to json", l.Any("json", err))
		return
	}
	fmt.Println(body)
}

// Verify user
// @Summary      Verify custumer
// @Description  Verifys custumer
// @Tags         Register
// @Security BearerAuth
// @Accept       json
// @Produce      json
// @Param        code   path string true "code"
// @Param        email  path string true "email"
// @Success      200  {object}  models.User
// @Router       /v1/verify/{email}/{code} [patch]
func (h *handlerV1) Verify(c *gin.Context) {
	var (
		code  = c.Param("code")
		email = c.Param("email")
	)
	fmt.Println(email, code)
	userBody, err := h.redis.Get(email)
	fmt.Println(err)
	if err != nil {
		c.JSON(http.StatusGatewayTimeout, gin.H{
			"info":  "Your time has expired",
			"error": err.Error(),
		})
		h.log.Error("Error while getting user from redis", l.Any("redis", err))
	}
	fmt.Println(userBody)
	//fmt.Printf(">>", userBody)
	userBodys := cast.ToString(userBody)

	body := us.CreateUserRequest{}
	fmt.Println(string(userBodys))
	err = json.Unmarshal([]byte(userBodys), &body)
	fmt.Println(body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Error while unmarshaling from json to user body", l.Any("json", err))
		return
	}
	if body.Code != code {
		c.JSON(http.StatusConflict, gin.H{
			"info": "Wrong code",
		})
		return
	}
	_, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	//id := strconv.FormatInt(int64(body.), 10)

	// Genrating refresh and jwt tokens
	h.jwthandler.Iss = body.Type
	h.jwthandler.Sub = *body.Username
	h.jwthandler.Role = "authorized"
	h.jwthandler.Aud = []string{"blog-app"}
	h.jwthandler.SigninKey = h.cfg.SignKey
	h.jwthandler.Log = h.log
	tokens, err := h.jwthandler.GenerateAuthJWT()
	accessToken := tokens[0]
	refreshToken := tokens[1]

	if err != nil {
		h.log.Error("error occured while generating tokens")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong,please try again",
		})
		return
	}
	fmt.Println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",body, accessToken, body.Password)
	res, err := h.storage.User().Create(&repo.User{
		FirstName:       body.FirstName,
		LastName:        body.LastName,
		PhoneNumber:     body.PhoneNumber,
		Email:           body.Email,
		Gender:          body.Gender,
		Password:        body.Password,
		Username:        body.Username,
		ProfileImageUrl: body.ProfileImageUrl,
		Type:            body.Type,
		AccessToken:     accessToken,
	})
	fmt.Println(err)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Error while creating user", l.Any("post", err))
		return
	}

	fmt.Println(refreshToken, "\n", "\n")
	fmt.Println(accessToken)

	c.JSON(http.StatusOK, res)
}

// Get user
// @Summary Get user
// @Description Get user
// @Tags	User
// @Security BearerAuth
// @Accept json
// @Produce json
// @Param  id   path  int  true  "User ID"
// @Success 200 {object} models.User
// @Router /v1/user/get/{id} [get]
func (h *handlerV1) GetUser(c *gin.Context) {
	guid := c.Param("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	result, err := h.storage.User().Get(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create user", l.Error(err))
		return
	}
	c.JSON(http.StatusOK, result)
}

// Get user by email
// @Summary Get user by email
// @Description Get user by email
// @Tags	User
// @Security BearerAuth
// @Accept json
// @Produce json
// @Param  email   path  string  true  "email"
// @Success 200 {object} models.User
// @Router /v1/user/getByEmail/{email} [get]
func (h *handlerV1) GetUserByEmail(c *gin.Context) {
	email := c.Param("email")
	result, err := h.storage.User().GetByEmail(email)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create user", l.Error(err))
		return
	}
	c.JSON(http.StatusOK, result)
}

// Get all User
// @Summary Get all users
// @Description Get all users
// @Tags User
// @Accept json
// @Produce json
// @Param 	filter body models.GetAllParams true "Filter"
// @Success 200 {object} models.GetAllUsersResponse
// @Router /v1/user/all [get]
//func (h *handlerV1) GetAllUsers(c *gin.Context) {
//	req, err := validateGetAllParams(c)
//	if err != nil {
//		c.JSON(http.StatusInternalServerError, gin.H{
//			"error": err.Error(),
//		})
//		h.log.Error("failed to get all user", l.Error(err))
//		return
//	}
//	result, err := h.storage.User().GetAll(&repo.GetAllUsersParams{
//		Page:   req.Page,
//		Limit:  req.Limit,
//		Search: req.Search,
//	})
//	if err != nil {
//		c.JSON(http.StatusInternalServerError, gin.H{
//			"error": err.Error(),
//		})
//		h.log.Error("failed to get all user", l.Error(err))
//		return
//	}
//	c.JSON(http.StatusOK, result)
//}
