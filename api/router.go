package api

import (
	v1 "gitlab.com/blog_project/api/handlers/v1"
	"gitlab.com/blog_project/config"
	"gitlab.com/blog_project/pkg/logger"
	//"gitlab.com/blog_project/services"
	"github.com/gin-contrib/cors"
	jwthandler "gitlab.com/blog_project/api/tokens"
	middleware "gitlab.com/blog_project/api/middleware"
	"gitlab.com/blog_project/storage/repo"
	"github.com/casbin/casbin/v2"

	_ "gitlab.com/blog_project/api/docs" //swag

	"github.com/gin-gonic/gin"
	swaggerFile "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// Option ...
type Option struct {
	Conf           config.Config
	Logger         logger.Logger
	Redis     repo.InMemoryStorageI
	CasbinEnforcer  *casbin.Enforcer
}

// New ...
// @Description Created by Abduazim Kabulov
// @securityDefinitions.apikey BearerAuth
// @in header
// @name Authorization
// @host      54.238.26.123:9079
// @BasePath  /v1

func New(option Option) *gin.Engine {
	router := gin.New()
	corConfig := cors.DefaultConfig()
  corConfig.AllowAllOrigins = true
  corConfig.AllowCredentials = true
  corConfig.AllowHeaders = []string{"*"}
  corConfig.AllowBrowserExtensions = true
  corConfig.AllowMethods = []string{"*"}


	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	jwt := jwthandler.JWTHandler{
		SigninKey: option.Conf.SignKey,
		Log:       option.Logger,
	}

	
	handlerV1 := v1.New(&v1.HandlerV1Config{
		Logger:         option.Logger,
		Cfg:            option.Conf,
		Redis:          option.Redis,
		JWTHandler:      jwt,
	})

	router.Use(middleware.NewAuth(option.CasbinEnforcer, jwt, config.Load()))


	api := router.Group("/v1")
	url := ginSwagger.URL("swagger/doc.json")
	api.POST("/register",handlerV1.RegisterUser)
	api.PATCH("/verify/:email/:code",handlerV1.Verify)
	api.GET(" /user/get:id", handlerV1.GetUser)
	api.GET(" /user/getByEmail/:email", handlerV1.GetUserByEmail)
	api.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFile.Handler, url))
	return router

}
